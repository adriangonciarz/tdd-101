import csv
import re
from typing import List

from src import config


class UnsupportedAmountTypeException(Exception):
    pass


class CarPrice:
    def __init__(self, amount: float, currency: str):
        if not isinstance(amount, float):
            raise UnsupportedAmountTypeException('CarPrice amount should be float')
        self.amount = amount
        self.currency = currency


class Car:
    def __init__(self, title_string: str, price_string: str):
        self.title_string = title_string.replace("-", "")
        self.price_string = price_string

    @property
    def brand(self):
        for brand in config.models_map.keys():
            if brand in self.title_string:
                return brand

    @property
    def model(self):
        models = config.models_map[self.brand]
        for model in models:
            if model in self.title_string:
                return model

    @property
    def price(self) -> CarPrice:
        return CarPrice(self._extract_amount(), self._extract_currency())

    def _extract_currency(self):
        for currency_key, currency_aliases in config.currency_map.items():
            if any((currency_alias in self.price_string for currency_alias in currency_aliases)):
                return currency_key

    def _extract_amount(self):
        spaceless_price = self.price_string.replace(' ', '')
        if ',' in spaceless_price and '.' in spaceless_price:
            return float(re.match('\D*(\d+.\d+)\D*', spaceless_price.replace(',', '')).group(1))
        elif '.' in spaceless_price:
            return float(re.match('\D*(\d+)\D*', spaceless_price.replace('.', '')).group(1))
        else:
            return float(re.match('\D*(\d+)\D*', spaceless_price).group(1))


class CarManager:
    def __init__(self):
        self.cars = []

    def load_from_csv(self, csv_path):
        with open(csv_path) as csvfile:
            csvreader = csv.DictReader(csvfile, delimiter=';')
            for row in csvreader:
                self.cars.append(Car(row['title'], row['price']))

    def get_cars(self) -> List[Car]:
        return self.cars
