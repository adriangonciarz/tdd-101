models_map = {
    'BMW': ('X6', 'M2', 'M3', 'X3'),
    'Hyundai': ('i30', 'i10', 'i20', 'Ionique'),
    'Mercedes': ('CLA', 'C-Klasse', 'E-Klasse'),
    'Audi': ('A1', 'A3', 'A4', 'S3', 'RS3', 'S4', 'RS4', 'RS8'),
}

currency_map = {
    'PLN': ('PLN', 'zł'),
    'USD': ('USD', '$'),
    'EUR': ('EUR', '€')
}