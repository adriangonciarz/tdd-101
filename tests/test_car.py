import pytest
from src.cars import Car


class TestCar:
    @pytest.mark.parametrize("title_string,expected_brand",
                             [('Awesome BMW X-6 like new!', 'BMW'),
                             ('Praktycznie nowe Audi S3 quattro bezwypadkowe!', 'Audi'),
                             ('Hyundai i30N 275 BHP, never in accident', 'Hyundai'),
                              ])
    def test_brand_recognition(self, title_string,expected_brand):
        test_car = Car(title_string, '10000 PLN')
        assert test_car.brand == expected_brand

    @pytest.mark.parametrize("title_string,expected_model",
                             [('Awesome BMW X-6 like new!', 'X6'),
                             ('Praktycznie nowe Audi S3 quattro bezwypadkowe!', 'S3'),
                             ('Hyundai i30N 275 BHP, never in accident', 'i30'),
                              ])
    def test_model_recognition(self, title_string,expected_model):
        test_car = Car(title_string, '10000 PLN')
        assert test_car.model == expected_model

    @pytest.mark.parametrize("price_string,expected_amount",
                             [('100 000 PLN', 100000.0),
                             ('3.999 €', 3999.0),
                             ('€ 2500', 2500),
                             ('US $75,000.00', 75000.),
                             ('US $12,000.50', 12000.5),
                              ])
    def test_car_price_amount_recognition(self, price_string,expected_amount):
        test_car = Car('BMW M2', price_string)
        assert test_car.price.amount == expected_amount

    @pytest.mark.parametrize("price_string,expected_currency",
                             [('100 000 PLN', 'PLN'),
                             ('3.999 €', 'EUR'),
                             ('€ 2500', 'EUR'),
                              ('US $75,000.00','USD'),
                              ])
    def test_car_price_currency_recognition(self, price_string,expected_currency):
        test_car = Car('BMW M2', price_string)
        assert test_car.price.currency == expected_currency
