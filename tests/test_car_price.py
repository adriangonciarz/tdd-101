import pytest

from src.cars import CarPrice, UnsupportedAmountTypeException


class TestCarPrice:
    def test_currency_storing(self):
        test_price = CarPrice(100.0, 'USD')
        assert test_price.currency == 'USD'

    def test_amount_storing(self):
        test_price = CarPrice(100.0, 'USD')
        assert test_price.amount == 100.0

    def test_currency_validation(self):
        with pytest.raises(UnsupportedAmountTypeException):
            CarPrice(50, 'PLN')
